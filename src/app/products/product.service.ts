import { compileNgModuleFromRender2 } from "@angular/compiler/src/render3/r3_module_compiler";
import { Injectable } from "@angular/core";
import { productos, proveedor } from "./products.model";

@Injectable({
  providedIn: "root",
})
export class ProductService {
  private proveedore: proveedor[] = [
    {
      nombre: "Intcomex",
      cedula: 11111,
      direccion: "San Jose",
      numero: 8888,
      correo: "www@deee.com",
      codigo: 333,
    },
    {
      nombre: "Intcomex",
      cedula: 11111,
      direccion: "San Jose",
      numero: 8888,
      correo: "www@deee.com",
      codigo: 333,
    },
  ];
  private productos: productos[] = [
    {
      proveedor: this.proveedore,
      precio: 888,
      candidad: 12,
      codigo: 1,
      nombre: "Tarjeta Madre",
      peso: 1112,
      fecha_caducidad: "11-11-1",
      descripcion: "Tarjeta madre socket AM4",
    },
    {
      proveedor: this.proveedore,
      precio: 888,
      candidad: 12,
      codigo: 2,
      nombre: "Tarjeta Madre",
      peso: 1113,
      fecha_caducidad: "11-11-1",
      descripcion: "Tarjeta madre socket Intel 1151",
    },
  ];
  constructor() {}
  getAll() {
    return [...this.productos];
  }

  getProduct(productId: number) {
    return {
      ...this.productos.find((product) => {
        return product.codigo === productId;
      }),
    };
  }

  deleteProduct(productId: number) {
    this.productos = this.productos.filter((product) => {
      return product.codigo !== productId;
    });
  }

  addProduct(
    pprecio: number,
    pcantidad: number,
    pcodigo: number,
    pnombre: string,
    ppeso: number,
    pfecha_caducidad: string,
    pdescripcion: string
  ) {
    const product: productos = {
      proveedor: this.proveedore,
      precio: pprecio,
      candidad: pcantidad,
      codigo: pcodigo,
      nombre: pnombre,
      peso: ppeso,
      fecha_caducidad: pfecha_caducidad,
      descripcion: pdescripcion,
    };
    this.productos.push(product);
  }

  editProduct(
    pprecio: number,
    pcantidad: number,
    pcodigo: number,
    pnombre: string,
    ppeso: number,
    pfecha_caducidad: string,
    pdescripcion: string
  ) {
    let index = this.productos.map((x) => x.codigo).indexOf(pcodigo);

    this.productos[index].codigo = pcodigo;
    this.productos[index].candidad = pcantidad;
    this.productos[index].nombre = pnombre;
    this.productos[index].peso = ppeso;
    this.productos[index].fecha_caducidad = pfecha_caducidad;
    this.productos[index].descripcion = pdescripcion;
    this.productos[index].precio = pprecio;

    /* let myObj = this.productos.find(ob => ob.codigo = pcodigo);

    myObj.candidad= pcantidad;
    myObj.precio= pprecio;
    myObj.nombre= pnombre;
    myObj.peso= ppeso;
    myObj.descripcion= pdescripcion;
    myObj.fecha_caducidad= pfecha_caducidad;

    this.productos[index] = myObj;*/

    console.log(this.productos);
  }
}
